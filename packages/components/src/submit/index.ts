import { h, useParentForm } from '@formily/vue'
import { IFormFeedback } from '@formily/core'
import { observer } from '@formily/reactive-vue'
import { defineComponent, toRef, watch } from 'vue'

import type { VxeButtonProps } from 'vxe-table'
import { Button as VxeButton } from 'vxe-table'

export interface ISubmitProps extends VxeButtonProps {
  onClick?: (e: MouseEvent) => any
  onSubmit?: (values: any) => any
  onSubmitSuccess?: (payload: any) => void
  onSubmitFailed?: (feedbacks: IFormFeedback[]) => void
}

export const Submit = observer(
  defineComponent({
    name: 'VSubmit',
    props: ['onClick', 'onSubmit', 'onSubmitSuccess', 'onSubmitFailed'],
    setup(props, { attrs, slots }) {
      const formRef = useParentForm()

      return () => {
        const {
          onClick = attrs?.click,
          onSubmit = attrs?.submit,
          onSubmitSuccess = attrs?.submitSuccess,
          onSubmitFailed = attrs?.submitFailed,
        } = props

        const form = formRef?.value
        return h(
          VxeButton,
          {
            attrs: {
              nativeType: attrs?.submit ? 'button' : 'submit',
              type: 'primary',
              ...attrs,
              loading:
                attrs.loading !== undefined ? attrs.loading : form?.submitting,
            },
            on: {
              ...attrs,
              click: (e: any) => {
                if (onClick) {
                  if (onClick(e) === false) return
                }
                if (onSubmit) {
                  form
                    ?.submit(onSubmit as (e: any) => void)
                    .then(onSubmitSuccess as (e: any) => void)
                    .catch(onSubmitFailed as (e: any) => void)
                }
              },
            },
          },
          slots
        )
      }
    },
  })
)

export default Submit
