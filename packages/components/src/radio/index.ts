import { connect, mapProps, h, mapReadPretty } from '@formily/vue'
import { defineComponent, PropType } from '@vue/composition-api'
import {
  composeExport,
  transformComponent,
  resolveComponent,
  SlotTypes,
} from '../__builtins__/shared'
import { PreviewText } from '../preview-text'
import type {
  VxeRadioProps, VxeRadioGroupProps, VxeRadioButtonProps
} from 'vxe-table'
import {
  Radio as VxeRadio,
  RadioGroup as VxeRadioGroup,
  RadioButton as VxeRadioButton,
} from 'vxe-table'

export type RadioGroupProps = VxeRadioGroupProps & {
  value: any
  options?: (
    | (Omit<VxeRadioProps, 'value'> & {
    value: VxeRadioProps['label']
    label: SlotTypes
  })
    | string
    )[]
  optionType: 'default' | 'button'
}

export type RadioProps = VxeRadioProps

const TransformRadioGroup = transformComponent(VxeRadioGroup, {
  change: 'update:modelValue',
})
const TransformRadio = connect(transformComponent(VxeRadio, {
  change: 'update:modelValue',
}), mapProps({title: 'content'}))


const RadioGroupOption = defineComponent<RadioGroupProps>({
  name: 'FRadioGroup',
  props: {
    options: {
      type: Array as PropType<RadioGroupProps['options']>,
      default: () => [],
    },
    optionType: {
      type: String as PropType<RadioGroupProps['optionType']>,
      default: 'default',
    },
  },
  setup(customProps, {attrs, slots, listeners}) {
    return () => {
      const options = customProps.options || []
      const OptionType =
        customProps.optionType === 'button' ? VxeRadioButton : VxeRadio
      const children =
        options.length !== 0
          ? {
            default: () =>
              options.map((option) => {
                if (typeof option === 'string') {
                  return h(
                    OptionType,
                    {props: {label: option}},
                    {
                      default: () => [
                        resolveComponent(slots?.option ?? option, {option}),
                      ],
                    }
                  )
                } else {
                  return h(
                    OptionType,
                    {
                      props: {
                        ...option,
                        value: undefined,
                        label: option.value,
                      },
                    },
                    {
                      default: () => [
                        resolveComponent(slots?.option ?? option.label, {
                          option,
                        }),
                      ],
                    }
                  )
                }
              }),
          }
          : slots
      return h(
        TransformRadioGroup,
        {
          attrs: {
            ...attrs,
          },
          on: listeners,
        },
        children
      )
    }
  },
})

const RadioGroup = connect(
  RadioGroupOption,
  mapProps({dataSource: 'options'}),
  mapReadPretty(PreviewText.Select)
)
export const Radio = composeExport(TransformRadio, {
  Group: RadioGroup,
})

export default Radio
