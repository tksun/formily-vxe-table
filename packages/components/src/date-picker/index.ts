import { connect, mapProps, mapReadPretty } from '@formily/vue'

import { Input, InputProps } from '../input'
import { PreviewText } from '../preview-text'

export type DatePickerProps =
  InputProps
  & { type: 'date' | 'time' | 'datetime' | 'week' | 'month' | 'quarter' | 'year' }

const getDefaultFormat = (props, formatType = 'format') => {
  const type = props.type
  
  if (type === 'week' && formatType === 'format') {
    return 'yyyy-WW'
  } else if (type === 'month') {
    return 'yyyy-MM'
  } else if (type === 'year') {
    return 'yyyy'
  } else if (type === 'datetime') {
    return 'yyyy-MM-dd HH:mm:ss'
  }
  
  return 'yyyy-MM-dd'
}

export const DatePicker = connect(
  Input,
  mapProps({readOnly: 'readonly'}, (props) => {
    return {
      type: 'date',
      ...props,
      format: props.format || getDefaultFormat(props),
      valueFormat: props.valueFormat || getDefaultFormat(props, 'valueFormat'),
    }
  }),
  mapReadPretty(PreviewText.DatePicker)
)

export default DatePicker
