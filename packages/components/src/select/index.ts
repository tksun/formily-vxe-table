import { connect, h, mapProps, mapReadPretty } from '@formily/vue'
import { defineComponent } from 'vue'
import { PreviewText } from '../preview-text'

import type { VxeOptionProps, VxeSelectProps } from 'vxe-table'
import { Option as VxeOption, Select as VxeSelect, Optgroup as VxeOptionGroup } from 'vxe-table'
import { resolveComponent, transformComponent } from '../__builtins__'
import { isArr } from "@formily/shared";

export type SelectProps = VxeSelectProps & {
  value: any
  options?: Array<VxeOptionProps>
}

const TransformSelect = transformComponent(VxeSelect, {
  change: 'update:modelValue',
})

const SelectOption = defineComponent<SelectProps>({
  name: 'FSelect',
  props: ['options'],
  setup(customProps, {attrs, slots, listeners, emit}) {
    return () => {
      const options = customProps.options || []
      
      const renderOptions = (options: VxeSelectProps[]) => {
        return options.map((option) => {
          if (typeof option === 'string') {
            return h(
              VxeOption,
              {props: {value: option, label: option}},
              {
                default:
                  slots?.option &&
                  (() => [resolveComponent(slots?.option, {option})]),
              }
            )
          } else if (isArr(option.options)) {
            return h(VxeOptionGroup, {props: {...option}}, {default: () => renderOptions(option.options)})
          } else {
            return h(
              VxeOption,
              {
                props: {
                  ...option,
                },
              },
              {
                default:
                  slots?.option &&
                  (() => [
                    resolveComponent(slots?.option, {
                      option,
                    }),
                  ]),
              }
            )
          }
        })
      }
      
      const children =
        options.length !== 0
          ? {
            default: () => renderOptions(options)
          }
          : slots
      return h(
        TransformSelect,
        {
          attrs: {
            ...attrs,
          },
          on: {
            ...listeners,
          },
        },
        children
      )
    }
  },
})

export const Select = connect(
  SelectOption,
  mapProps({dataSource: 'options', loading: true}),
  mapReadPretty(PreviewText.Select)
)

export default Select
