import { h } from '@formily/vue'
import { defineComponent, toRef } from 'vue'
import type { VxeSwitchProps as VxwSwitchProps } from 'vxe-table'
import { Switch as VxeSwitch } from 'vxe-table'
import { transformComponent } from '../__builtins__'

export type SwitchProps = VxwSwitchProps

export const Switch =
  transformComponent(VxeSwitch, { change: 'update:modelValue' }) ||
  defineComponent({
    name: 'VSwitch',
    props: { value: {} },
    emits: ['change'],
    setup(props, { attrs, emit }) {
      const valueRef = toRef(props, 'value')
      return () => {
        return h(
          VxeSwitch,
          {
            props: { modelValue: valueRef.value },
            attrs,
            on: {
              'update:modelValue': (...args) => {
                emit('change', ...args)
              },
            },
          },
          {}
        )
      }
    },
  })
export default Switch
