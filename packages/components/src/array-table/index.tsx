import {
  defineComponent,
  inject,
  InjectionKey,
  onBeforeUnmount,
  ref,
  Ref,
  shallowRef,
  watch,
  markRaw,
  provide,
} from 'vue'

import { observe } from '@formily/reactive'
import {
  createForm,
  GeneralField,
  IVoidFieldFactoryProps,
  FieldDisplayTypes,
  ArrayField,
  onFieldMount,
  ObjectField,
} from '@formily/core'
import {
  useField,
  useFieldSchema,
  RecursionField as _RecursionField,
  h,
  Fragment,
  useForm,
} from '@formily/vue'
import { observer } from '@formily/reactive-vue'
import { FormPath, isArr, isBool } from '@formily/shared'
import { ArrayBase } from '../array-base'
import { stylePrefix } from '../__builtins__/configs'
import { composeExport } from '../__builtins__/shared'
import { Schema } from '@formily/json-schema'
import type {
  VxeGridInstance,
  VxeTableDefines,
  VxeTableProps as TableProps,
  VxeColumnProps,
  VxeColgroupProps,
  VxePagerProps as PaginationProps,
  VxeOptgroupProps,
  VxeGridProps,
} from 'vxe-table'
import type { VNode, Component } from 'vue'
import {
  Table,
  Column,
  Colgroup,
  Pager,
  Select,
  Option,
  Optgroup,
  Grid,
} from 'vxe-table'

function useDomainModel<T>() {
  return inject<Ref<T>>(Symbol.for('domainmodel'))
}

export const VXETablePropsSymbol: InjectionKey<Ref<Partial<VxeGridProps>>> =
  Symbol('VXE-TABLE-PROPS')
const VXETableInstanceSymbol: InjectionKey<Ref<VxeGridInstance>> =
  Symbol('VXE-TABLE-INSTANCE')

// 获取table的实例
export function useTableInstance(): Ref<VxeGridInstance> {
  const instance = ref<VxeGridInstance>(null)
  provide(VXETableInstanceSymbol, instance)
  return instance
}

const RecursionField = _RecursionField as unknown as Component

interface IArrayTableProps extends TableProps {
  pagination?: PaginationProps | boolean
}
interface IArrayTablePaginationProps extends PaginationProps {
  dataSource?: any[]
}

interface ObservableColumnSource {
  field: GeneralField
  fieldProps: IVoidFieldFactoryProps<any, any>
  columnProps: VxeColumnProps & { title: string; asterisk: boolean }
  schema: Schema
  display: FieldDisplayTypes
  required: boolean
  name: string
  children: ObservableColumnSource[]
}

type ColumnProps = VxeColumnProps & {
  key: string | number
  asterisk: boolean
  render?: (props: {
    row: Record<string, any>
    column: VxeColumnProps
    $index: number
  }) => VNode
}

const isColumnComponent = (schema: Schema) => {
  return schema['x-component']?.indexOf('Column') > -1
}

const isColgroupComponent = (schema: Schema) => {
  return schema['x-component']?.indexOf('Colgroup') > -1
}

const isOperationsComponent = (schema: Schema) => {
  return schema['x-component']?.indexOf('Operations') > -1
}

const isAdditionComponent = (schema: Schema) => {
  return schema['x-component']?.indexOf('Addition') > -1
}

const getArrayTableSources = (
  arrayFieldRef: Ref<GeneralField>,
  schemaRef: Ref<Schema>
) => {
  const arrayField = arrayFieldRef.value
  const parseSources = (
    schema: Schema,
    arrayField: GeneralField
  ): ObservableColumnSource[] => {
    if (
      isColumnComponent(schema) ||
      isOperationsComponent(schema) ||
      isAdditionComponent(schema) ||
      isColgroupComponent(schema)
    ) {
      // TODO::这里prop来指定字段对应的字段 应该换成field
      if (!schema['x-component-props']?.['prop'] && !schema['name']) return []
      const name = schema['x-component-props']?.['prop'] || schema['name']
      const field = arrayField?.query(arrayField.address.concat(name)).take()
      const fieldProps = field?.props || schema.toFieldProps()
      const columnProps =
        (field?.component as any[])?.[1] || schema['x-component-props'] || {}
      const display = field?.display || schema['x-display']
      const required = schema.reduceProperties((required, property) => {
        if (required) {
          return required
        }
        return !!property.required
      }, false)

      return [
        {
          name,
          display,
          required,
          field,
          fieldProps,
          schema,
          columnProps,
          children: isColgroupComponent(schema)
            ? parseSources({ ...schema, 'x-component': null }, field)
            : undefined,
        },
      ]
    } else if (schema.properties) {
      return schema.reduceProperties((buf: any[], schema) => {
        return buf.concat(parseSources(schema, arrayField))
      }, [])
    } else {
      return []
    }
  }

  const parseArrayTable = (schema: Schema['items']) => {
    if (!schema) return []
    const sources: ObservableColumnSource[] = []
    const items = isArr(schema) ? schema : ([schema] as Schema[])
    return items.reduce((columns, schema) => {
      const item = parseSources(schema, arrayField)
      if (item) {
        return columns.concat(item)
      }
      return columns
    }, sources)
  }

  if (!schemaRef.value) throw new Error('can not found schema object')

  return parseArrayTable(schemaRef.value.items)
}
// 返回grid需要的json
const getArrayTableColumns = (
  reactiveDataSource: Ref<any[]>,
  sources: ObservableColumnSource[]
): VxeGridProps['columns'] => {
  return sources?.reduce(
    (
      buf: VxeTableDefines.ColumnOptions[],
      {
        name,
        display,
        required,
        field,
        fieldProps,
        schema,
        columnProps,
        children,
      },
      key
    ) => {
      const { title, asterisk, ...props } = columnProps
      if (display !== 'visible') return buf
      if (!isColumnComponent(schema) && !isColgroupComponent(schema)) return buf
      const slots = {}
      // 渲染content   // TODO::其他的插槽在需要的时候
      // if (!ColumnPorps?.type == 'radio') {} //TODO::把radio头部变成取消单选
      if (!columnProps?.type) {
        slots.default = (props) => {
          const {
            row,
            rowIndex,
            $rowIndex,
            column,
            columnIndex,
            $columnIndex,
            _columnIndex,
          } = props
          // TODO::用了相同的key理应当进行更新的
          return (
            <ArrayBase.Item
              record={row}
              index={rowIndex}
              key={`${key}${rowIndex}`}
            >
              <RecursionField
                schema={schema}
                name={rowIndex}
                onlyRenderProperties={true}
              ></RecursionField>
            </ArrayBase.Item>
          )
        }
      }
      return buf.concat({
        field: name,
        title: title || schema.title,
        children: getArrayTableColumns(reactiveDataSource, children),
        ...props,
        slots,
      })
    },
    []
  )
}

const ArrayTablePagination = observer(
  defineComponent({
    props: { value: { type: Object, default: () => ({}) } },
    setup(props, { attrs, slots, emit }) {
      const prefixCls = `${stylePrefix}-array-table`
      const domainModelRef = useDomainModel<any>()
      const fieldRef = useField<ObjectField>()
      return () => {
        const domainModel = domainModelRef.value
        const pagerConfig = domainModelRef.value.pagerConfig
        fieldRef.value.setDataSource(pagerConfig)
        return (
          <Pager
            {...attrs}
            currentPage={pagerConfig.currentPage}
            pageSize={pagerConfig.pageSize}
            total={pagerConfig.total}
            onPageChange={domainModel.handlePagerChange}
          />
        )
      }
    },
  })
)

const ArrayTableInner = defineComponent<IArrayTableProps>({
  name: 'FArrayTable',
  inheritAttrs: false,
  setup(props, { attrs, slots }) {
    const fieldRef = useField<ArrayField>()
    const schemaRef = useFieldSchema()
    const prefixCls = `${stylePrefix}-array-table`
    const { getKey, keyMap } = ArrayBase.useKey(schemaRef.value)

    // 处理外部传进来的事件监听等信息
    const additionalPropsRef = inject(VXETablePropsSymbol, ref({} as any))
    // 外部传递过来用于绑定table实例
    const instace = inject(VXETableInstanceSymbol, ref())

    const defaultRowKey = (record: any) => {
      return getKey(record)
    }

    const reactiveDataSource = shallowRef([])
    const dispose = observe(
      fieldRef.value,
      () => {
        reactiveDataSource.value = fieldRef.value.value
      },
      false
    )
    onBeforeUnmount(dispose)

    return () => {
      const props = attrs as unknown as IArrayTableProps
      const field = fieldRef.value
      const dataSource = Array.isArray(field.value) ? field.value.slice() : []
      const pagination = props.pagination
      const sources = getArrayTableSources(fieldRef, schemaRef)
      const columns = getArrayTableColumns(reactiveDataSource, sources)
      const injectedProps = additionalPropsRef.value
      //专门用来承接对Column的状态管理 可以操作columns的field对象
      const renderStateManager = (columns, baseName?) =>
        columns.reduce((acc, column, key) => {
          column = { ...column }
          if (baseName) column.name = `${baseName}.${column.name}`
          if (isColgroupComponent(column.schema)) {
            // 添加自己和对应的子节点
            return acc.concat(
              <RecursionField
                name={column.name}
                schema={column.schema}
              ></RecursionField>,
              renderStateManager(column.children, column.name)
            )
          }
          if (!isColumnComponent(column.schema)) return acc
          return acc.concat(
            <RecursionField
              name={column.name}
              schema={column.schema}
            ></RecursionField>
          )
        }, [])

      // 这里处理grid组件的插槽 这个bottom 里面是为了构造formily的插槽
      const _slots = schemaRef.value.reduceProperties(
        (buffer, schema, key, index) => {
          if (schema['x-component']?.includes('Toolbar')) {
            buffer['toolbar'] = () => (
              <RecursionField name={key} schema={schema}></RecursionField>
            )
          } else if (schema['x-component']?.includes('Empty')) {
            buffer['empty'] = () => (
              <RecursionField name={key} schema={schema}></RecursionField>
            )
          } else if (schema['x-component']?.includes('Form')) {
            buffer['form'] = () => (
              <RecursionField name={key} schema={schema}></RecursionField>
            )
          } else if (schema['x-component']?.includes('Top')) {
            buffer['top'] = () => (
              <RecursionField name={key} schema={schema}></RecursionField>
            )
          } else if (schema['x-component']?.includes('Pager')) {
            buffer['pager'] = () => (
              <RecursionField name={key} schema={schema}></RecursionField>
            )
          }
          return buffer
        },
        { bottom: () => renderStateManager(sources) }
      )

      return (
        <ArrayBase keyMap={keyMap}>
          <Grid
            ref={instace}
            v-slots={{ ..._slots, ...slots }}
            syncResize
            autoResize={true}
            columns={columns}
            data={dataSource}
            {...{ ...injectedProps, ...field.componentProps }}
          ></Grid>
        </ArrayBase>
      )
    }
  },
})

const ArrayTableColumn: Component = {
  name: 'FArrayTableColumn',
  inheritAttrs: false,
  render(h) {
    return <span></span>
  },
}
const ArrayTableColumnGroup: Component = {
  name: 'FArrayTableColumnGroup',
  inheritAttrs: false,
  render(h) {
    return <span></span>
  },
}
export const ArrayTable = composeExport(ArrayTableInner, {
  Column: ArrayTableColumn,
  Colgroup: ArrayTableColumnGroup,
  Toolbar: (props, context) => context.slots?.default(),
  Empty: (props, context) => context.slots?.default(),
  Form: (props, context) => context.slots?.default(),
  Top: (props, context) => context.slots?.default(),
  Pager: ArrayTablePagination,
  // Index: ArrayBase.Index,
  // SortHandle: ArrayBase.SortHandle,
  // Addition: ArrayBase.Addition,
  // Remove: ArrayBase.Remove,
  // MoveDown: ArrayBase.MoveDown,
  // MoveUp: ArrayBase.MoveUp,
  // useArray: ArrayBase.useArray,
  // useIndex: ArrayBase.useIndex,
  // useRecord: ArrayBase.useRecord,
})

export default ArrayTable
