import './style.scss'

import 'vxe-table/styles/table.scss'
import 'vxe-table/styles/colgroup.scss'
import 'vxe-table/styles/column.scss'
import 'vxe-table/styles/pager.scss'
import 'vxe-table/styles/select.scss'
import 'vxe-table/styles/optgroup.scss'
import 'vxe-table/styles/option.scss'

// 依赖
import '../array-base/style'
import '../space/style'
