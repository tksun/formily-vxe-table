import { Input } from '../input'
import { connect, mapProps, mapReadPretty } from '@formily/vue'
import { PreviewText } from '../preview-text'
import type { VxeInputProps } from 'vxe-table'

export type PasswordProps = VxeInputProps & { type: 'password' }

export const Password = connect(
  Input,
  mapProps((props) => {
    return {
      ...props,
      type: 'password',
    }
  }),
  mapReadPretty(PreviewText.Input)
)

export default Password
