import { composeExport, transformComponent } from '../__builtins__/shared'
import { connect, mapProps, mapReadPretty } from '@formily/vue'
import { PreviewText } from '../preview-text'
import type { VxeInputProps } from 'vxe-table'
import { Input as VxeInput, Textarea as VxeTextarea } from 'vxe-table'
import { defineComponent, h } from 'vue'

export type InputProps = VxeInputProps

const TransformInput = transformComponent(VxeInput, {
  change: 'update:modelValue',
})

const InnerInput = connect(
  TransformInput,
  mapProps({ readOnly: 'readonly' }),
  mapReadPretty(PreviewText.Input)
)

const TransformTextarea = transformComponent(VxeTextarea, {
  change: 'update:modelValue',
})

const TextArea = connect(
  TransformTextarea,
  mapProps((props) => {
    return {
      ...props,
    }
  }),
  mapReadPretty(PreviewText.Input)
)

export const Input = composeExport(InnerInput, {
  TextArea,
})

export default Input
