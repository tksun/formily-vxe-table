import { inject, ComputedRef, computed, provide } from 'vue'
export type SizeType = null | 'medium' | 'small' | 'mini'

const sizemap = {
  small: 'mini',
  default: 'small',
  large: 'medium',
}

export function useSize(props: { size: SizeType }) {
  // 组件尺寸上下文
  const xesize = inject('xesize', null as ComputedRef<SizeType> | null)
  const computeSize = computed(() => {
    return sizemap[props.size] || (xesize ? xesize.value : null)
  })
  provide('xesize', computeSize)

  return computeSize
}
