import type { Component } from 'vue'
import { merge } from '@formily/shared'
import { h } from '@formily/vue'
import { defineComponent } from 'vue-demi'

type ListenersTransformRules = Record<string, string>

export const transformComponent = <T extends Record<string, any>>(
  tag: any,
  transformRules?: ListenersTransformRules,
  defaultProps?: Partial<T>
): Component<T> | any => {
  return defineComponent({
    inheritAttrs: false,
    setup(props, { attrs, slots, emit }) {
      return () => {
        let data = {
          ...attrs,
        }
        if (attrs.value) {
          data['modelValue'] = attrs.value
        }
        if (transformRules) {
          const listeners = transformRules
          Object.keys(listeners).forEach((extract) => {
            const event = listeners[extract]
            data[`on${event[0].toUpperCase()}${event.slice(1)}`] =
              attrs[`on${extract[0].toUpperCase()}${extract.slice(1)}`]
          })
          if (Object.keys(listeners).includes('change')) {
            delete data['onChange']
          }
        }
        if (defaultProps) {
          data = merge(defaultProps, data)
        }
        return h(tag, data, slots)
      }
    },
  })
}
