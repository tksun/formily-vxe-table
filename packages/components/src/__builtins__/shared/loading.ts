import { VXETable } from 'vxe-table'
import { uid } from '@formily/shared'

export const loading = async (
  loadingText = '加载中...',
  processor: () => Promise<any>
) => {
  const uuid = uid()
  let loading = setTimeout(() => {
    VXETable.modal.message({
      mask: true,
      showClose: false,
      title: '加载中',
      content: loadingText,
      status: 'loading',
      id: uuid,
      duration: -1
    })
  }, 100)
  try {
    return await processor()
  } finally {
    await VXETable.modal.close(uuid)
    clearTimeout(loading)
  }
}
