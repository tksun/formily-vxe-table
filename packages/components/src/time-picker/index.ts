import { connect, mapProps, mapReadPretty } from '@formily/vue'
import { PreviewText } from '../preview-text'
import { Input, InputProps } from '../input'

export type TimePickerProps = InputProps & { type: 'time' }

export const TimePicker = connect(
  Input,
  mapProps({readOnly: 'readonly'}, (props => {
    return {type: 'time', ...props}
  })),
  mapReadPretty(PreviewText.TimePicker)
)

export default TimePicker
