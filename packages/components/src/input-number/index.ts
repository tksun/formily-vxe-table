import { transformComponent } from '../__builtins__/shared'
import { connect, mapProps, mapReadPretty } from '@formily/vue'
import { Input, InputProps } from '../input'

import { PreviewText } from '../preview-text'

export type InputNumberProps = InputProps

export const InputNumber = connect(
  Input,
  mapProps({ readOnly: 'readonly' }, (props) => {
    return {
      type: 'number',
      ...props,
    }
  }),
  mapReadPretty(PreviewText.Input)
)

export default InputNumber
