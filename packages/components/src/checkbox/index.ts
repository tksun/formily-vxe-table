import { connect, mapProps, h, mapReadPretty } from '@formily/vue'
import { defineComponent, PropType } from 'vue'
import {
  composeExport,
  transformComponent,
  resolveComponent,
  SlotTypes,
} from '../__builtins__/shared'
import type {
  VxeCheckboxProps as _VxeCheckboxProps,
  VxeCheckboxGroupProps,
} from 'vxe-table'
import {
  Checkbox as VxeCheckbox,
  CheckboxGroup as VxeCheckboxGroup,
} from 'vxe-table'
import { PreviewText } from '../preview-text'

const TransformCheckbox = transformComponent(VxeCheckbox, {
  change: 'update:modelValue',
})
const TransformCheckboxGroup = transformComponent(VxeCheckboxGroup, {
  change: 'update:modelValue',
})

type VxeCheckboxProps = Omit<_VxeCheckboxProps, 'value'> & {
  value: VxeCheckboxProps['label']
}

export interface CheckboxProps extends VxeCheckboxProps {
  option: Omit<_VxeCheckboxProps, 'value'> & {
    value: VxeCheckboxProps['label']
    label: SlotTypes
  }
}
const CheckboxOption = defineComponent<CheckboxProps>({
  name: 'FCheckbox',
  props: {
    option: {
      type: Object,
      default: null,
    },
  },
  setup(curtomProps, { attrs, slots, listeners, emit }) {
    return () => {
      const props = attrs as unknown as CheckboxProps
      const option = curtomProps?.option
      if (option) {
        const children = {
          default: () => [
            resolveComponent(slots.default ?? option.label, { option }),
          ],
        }
        const newProps = {} as Partial<VxeCheckboxProps>
        Object.assign(newProps, option)
        newProps.label = option.value
        delete newProps.value

        return h(
          TransformCheckbox,
          {
            attrs: {
              ...newProps,
            },
          },
          children
        )
      }

      return h(
        TransformCheckbox,
        {
          attrs: {
            ...props,
          },
        },
        slots
      )
    }
  },
})

export type CheckboxGroupProps = VxeCheckboxGroupProps & {
  value: any[]
  options?: Array<CheckboxProps | string>
}

const VxeCheckboxGroupInner = defineComponent<CheckboxGroupProps>({
  name: 'FCheckboxGroup',
  props: {
    options: {
      type: Array,
      default: () => [],
    },
  },
  setup(customProps, { attrs, slots, listeners, emit }) {
    return () => {
      const options = customProps.options || []
      const children =
        options.length !== 0
          ? {
              default: () =>
                options.map((option) => {
                  if (typeof option === 'string') {
                    return h(
                      Checkbox,
                      {
                        props: {
                          option: {
                            label: option,
                            value: option,
                          },
                        },
                        attrs: {
                          optionType: customProps.optionType,
                        },
                      },
                      slots?.option
                        ? { default: () => slots.option({ option }) }
                        : {}
                    )
                  } else {
                    return h(
                      Checkbox,
                      {
                        props: {
                          option,
                        },
                        attrs: {},
                      },
                      slots?.option
                        ? { default: () => slots.option({ option }) }
                        : {}
                    )
                  }
                }),
            }
          : slots
      return h(
        TransformCheckboxGroup,
        {
          attrs: {
            ...attrs,
          },
          on: listeners,
        },
        children
      )
    }
  },
})

const CheckboxGroup = connect(
  VxeCheckboxGroupInner,
  mapProps({ dataSource: 'options' }),
  mapReadPretty(PreviewText.Select, {
    multiple: true,
  })
)

export const Checkbox = composeExport(connect(CheckboxOption), {
  Group: CheckboxGroup,
})
