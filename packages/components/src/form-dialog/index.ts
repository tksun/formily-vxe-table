import { h, FormProvider, Fragment } from '@formily/vue'
import { toJS } from '@formily/reactive'
import * as Reactive from '@formily/reactive'
import { createForm, Form, IFormProps } from '@formily/core'
import {
  isNum,
  isStr,
  isBool,
  isFn,
  IMiddleware,
  applyMiddleware,
} from '@formily/shared'

import { Modal, Button } from 'vxe-table'
import type { VxeModalProps, VxeButtonProps } from 'vxe-table'
import { Component, VNode, createVNode, defineComponent, render as vueRender, Teleport, ref } from 'vue'
import {
  isValidElement,
  resolveComponent,
  createPortalProvider,
  getProtalContext,
  loading,
  computed as polyfillComputed
} from '../__builtins__'
import { stylePrefix } from '../__builtins__'

type FormDialogContentProps = { form: Form }

type FormDialogContent = Component | ((props: FormDialogContentProps) => VNode)

type DialogTitle = string | number | Component | VNode | (() => VNode)

type IFormDialogProps = Omit<VxeModalProps, 'title'> & {
  title?: DialogTitle
  footer?: null | Component | VNode | (() => VNode)
  cancelText?: string | Component | VNode | (() => VNode)
  cancelButtonProps?: VxeButtonProps
  okText?: string | Component | VNode | (() => VNode)
  okButtonProps?: VxeButtonProps
  onCancel?: () => void
  onOK?: () => void
  loadingText?: string
}

const PORTAL_TARGET_NAME = 'FormDialogFooter'

const isDialogTitle = (props: any): props is DialogTitle => {
  return isNum(props) || isStr(props) || isBool(props) || isValidElement(props)
}

const getDialogProps = (props: any): IFormDialogProps => {
  if (isDialogTitle(props)) {
    return {
      title: props,
    } as IFormDialogProps
  } else {
    return props
  }
}

export interface IFormDialog {
  forOpen(middleware: IMiddleware<IFormProps>): IFormDialog
  
  forConfirm(middleware: IMiddleware<IFormProps>): IFormDialog
  
  forCancel(middleware: IMiddleware<IFormProps>): IFormDialog
  
  open(props?: IFormProps): Promise<any>
  
  close(): void
}

export interface IFormDialogComponentProps {
  content: FormDialogContent
  resolve: () => any
  reject: () => any
}

export function FormDialog(
  title: IFormDialogProps | DialogTitle,
  content: FormDialogContent
): IFormDialog

export function FormDialog(
  title: IFormDialogProps | DialogTitle,
  id: string | symbol,
  content: FormDialogContent
): IFormDialog

export function FormDialog(
  title: DialogTitle,
  id: string,
  content: FormDialogContent
): IFormDialog

export function FormDialog(
  title: IFormDialogProps | DialogTitle,
  id: string | symbol | FormDialogContent,
  content?: FormDialogContent
): IFormDialog {
  if (isFn(id) || isValidElement(id)) {
    content = id as FormDialogContent
    id = 'form-dialog'
  }
  
  const prefixCls = `${stylePrefix}-form-dialog`
  const env = {
    root: document.createElement('div'),
    form: null,
    promise: null,
    instance: null,
    openMiddlewares: [],
    confirmMiddlewares: [],
    cancelMiddlewares: [],
  }
  
  document.body.appendChild(env.root)
  
  const props = getDialogProps(title)
  
  const dialogProps = {
    ...props,
    onClose: () => {
      props.onClosed?.()
      vueRender(null, env.root)
      env.instance = null
      env.root?.parentNode?.removeChild(env.root)
      env.root = undefined
    },
  }
  
  const component =
    defineComponent({
      setup() {
        return () => resolveComponent(content, {
          form: env.form,
        })
      },
    })
  
  const render = (visible = true, resolve?: () => any, reject?: () => any) => {
    if (!env.instance) {
      const ComponentConstructor =
        defineComponent({
          inheritAttrs: false,
          props: ['dialogProps'],
          setup() {
            return {
              visible: ref(false),
              loading: polyfillComputed(() => env.form.submitting)
            }
          },
          render() {
            const {
              onOK,
              onCancel,
              title,
              footer,
              okText,
              cancelText,
              okButtonProps,
              cancelButtonProps,
              ...dialogProps
            } = this.dialogProps
            
            return h(
              FormProvider,
              {
                props: {
                  form: env.form,
                },
              },
              {
                default: () =>
                  h(
                    Modal,
                    {
                      ref: 'modal',
                      attrs: {
                        className: `${prefixCls}`,
                        modelValue: this.visible,
                        showFooter: true,
                        ...dialogProps,
                      },
                      on: {
                        'update:modelValue': (val) => {
                          this.visible = val
                        },
                      },
                    },
                    {
                      default: () => [h(component, {}, {})],
                      title: () =>
                        h(
                          'div',
                          {},
                          {default: () => resolveComponent(title)}
                        ),
                      footer: () =>
                        h(
                          'div',
                          {},
                          {
                            default: () => {
                              
                              if (footer === null) {
                                return [null, FooterProtalTarget]
                              } else if (footer) {
                                return [
                                  resolveComponent(footer),
                                  FooterProtalTarget,
                                ]
                              }
                              
                              return [
                                h(
                                  Button,
                                  {
                                    attrs: cancelButtonProps,
                                    on: {
                                      click: (e) => {
                                        onCancel?.(e)
                                        reject()
                                      },
                                    },
                                  },
                                  {
                                    default: () =>
                                      resolveComponent(
                                        cancelText || '取消'
                                        // t('el.popconfirm.cancelButtonText')
                                      ),
                                  }
                                ),
                                h(
                                  Button,
                                  {
                                    attrs: {
                                      type: 'primary',
                                      ...okButtonProps,
                                      loading: this.loading,
                                    },
                                    on: {
                                      click: (e) => {
                                        onOK?.(e)
                                        resolve()
                                      },
                                    },
                                  },
                                  {
                                    default: () =>
                                      resolveComponent(
                                        okText || '确认'
                                        // t('el.popconfirm.confirmButtonText')
                                      ),
                                  }
                                ),
                                h(
                                  'div',
                                  {
                                    attrs: {
                                      id: PORTAL_TARGET_NAME
                                    },
                                    props: {
                                      slim: true,
                                    },
                                  },
                                  {}
                                ),
                              ]
                            },
                          }
                        ),
                    }
                  ),
              }
            )
          },
        })
      env.instance = createVNode(ComponentConstructor, {dialogProps})
      vueRender(env.instance, env.root)
    }
    // 用来更新这个东西
    env.instance.component.proxy.visible = visible
    if (!visible) {
      env.instance.component.proxy.$refs.modal.$emit('close')
    }
  }
  
  const formDialog = {
    forOpen: (middleware: IMiddleware<IFormProps>) => {
      if (isFn(middleware)) {
        env.openMiddlewares.push(middleware)
      }
      return formDialog
    },
    forConfirm: (middleware: IMiddleware<Form>) => {
      if (isFn(middleware)) {
        env.confirmMiddlewares.push(middleware)
      }
      return formDialog
    },
    forCancel: (middleware: IMiddleware<Form>) => {
      if (isFn(middleware)) {
        env.cancelMiddlewares.push(middleware)
      }
      return formDialog
    },
    open: (props: IFormProps) => {
      if (env.promise) return env.promise
      
      env.promise = new Promise(async (resolve, reject) => {
        try {
          props = await loading(dialogProps.loadingText, () =>
            applyMiddleware(props, env.openMiddlewares)
          )
          env.form = env.form || createForm(props)
        } catch (e) {
          reject(e)
        }
        
        render(
          true,
          () => {
            env.form
              .submit(async () => {
                await applyMiddleware(env.form, env.confirmMiddlewares)
                resolve(toJS(env.form.values))
                if (dialogProps.beforeHideMethod) {
                  setTimeout(async () => {
                    await dialogProps.beforeHideMethod(null)
                    formDialog.close()
                  })
                } else {
                  formDialog.close()
                }
              })
              .catch(reject)
          },
          async () => {
            await loading(dialogProps.loadingText, () =>
              applyMiddleware(env.form, env.cancelMiddlewares)
            )
            
            if (dialogProps.beforeHideMethod) {
              await dialogProps.beforeHideMethod(null)
              formDialog.close()
            } else {
              formDialog.close()
            }
          }
        )
      })
      return env.promise
    },
    close: () => {
      if (!env.root) return
      render(false)
    },
  }
  return formDialog
}

const FormDialogFooter = defineComponent({
  name: 'FFormDialogFooter',
  setup(props, {slots}) {
    return () => {
      return h(
        Teleport,
        {
          props: {
            to: '#' + PORTAL_TARGET_NAME,
          },
        },
        slots
      )
    }
  },
})

FormDialog.Footer = FormDialogFooter
FormDialog.Portal = createPortalProvider('form-dialog')

export default FormDialog
