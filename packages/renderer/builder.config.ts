import { IBuilderConfig } from '@formily/template'

export const BuilderConfig: IBuilderConfig = {
  targetLibName: 'vxe',
  targetLibCjsDir: 'lib',
  targetLibEsDir: 'es',
}
