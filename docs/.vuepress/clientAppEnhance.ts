// import pageComponents from '@internal/page-components'
import component from './components/dumi-previewer.vue'
import { defineClientAppEnhance } from '@vuepress/client'
import { createPinia } from 'pinia'
import VXETable from 'vxe-table'
import 'vxe-table/styles/index.scss'
import '@formily/vxe-table/src/space/style.scss';
import '@formily/vxe-table/src/form-item/style.scss';
import '@formily/vxe-table/src/form-grid/style.scss';
import { observable } from '@formily/reactive'
import { markRaw, toRaw } from 'vue'
const pinia = createPinia()

// 同步pinia的状态为formily的状态
pinia.use(({ options, store }) => {
  const $$state = markRaw(observable(options.state()))
  store.$subscribe((callback) => {
    const state = toRaw(store.$state)
    Object.keys(state).forEach(key => {
      $$state[key] = state[key]
    })
  })
  return {
    observable: $$state
  }
})

export default defineClientAppEnhance(({ app, router, siteData }) => {
  app.use(VXETable)
  app.use(pinia)
  app.component(component.name, component)
  // for (const [name, component] of Object.entries(pageComponents)) {
  //   app.component(name, component)
  // }
})
