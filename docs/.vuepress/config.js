const path = require('path')
const utils = require('./util')

const componentFiles = utils
  .getFiles(path.resolve(__dirname, '../guide'))
  .map((item) => item.replace(/(\.md)/g, ''))
  .filter((item) => {
    return ![
      'array-cards',
      'array-collapse',
      'array-tabs',
      'cascader',
      'editable',
      'form-drawer',
      'form-collapse',
      'form-tab',
      'form-step',
      'transfer',
      'upload',
      'el-form',
      'el-form-item',
      'index',
    ].includes(item)
  })

module.exports = {
  title: 'Formily VxeTable',
  dest: './doc-site',
  head: [
    [
      'link',
      {
        rel: 'icon',
        href: '//img.alicdn.com/imgextra/i3/O1CN01XtT3Tv1Wd1b5hNVKy_!!6000000002810-55-tps-360-360.svg',
      },
    ],
  ],
  bundler: '@vuepress/bundler-webpack',
  bundlerConfig: {
    chainWebpack: (config) => {
      // node_modules\@vuepress\bundler-webpack\lib\dev\createDevServerConfig.js 添加overlay=false
      config.devServer.set('client', { overlay: { errors: false, warnings: false } })
      console.log(config.toString())
      config.resolve.alias.set(
        'vue-demi',
        path.resolve(__dirname, '../../node_modules/vue-demi/lib/v3/index.mjs')
      )
      config.resolve.alias.set(
        '@formily/reactive-vue',
        path.resolve(__dirname, '../../node_modules/@formily/reactive-vue/lib/index.js')
      )
      // config.resolve.alias.set('@formily/reactive-vue', path.resolve(__dirname, '../../reactive-vue/index.js'))

      // 改为用babel打包
      config.module
        .rule('jsxvue')
        .test(/\.m?[jt]sx?$/)
        .use('babel-loader')
        .loader('babel-loader')
        .options({
          presets: [['@babel/preset-env', { targets: { chrome: 98 } }], 'babel-preset-typescript-vue3'],
          plugins: [
            ["@vue/babel-plugin-jsx", { transformOn: true, mergeProps: true }],
            "@babel/plugin-transform-typescript",
          ]
        })
        .end()

      config.module
        .rule('thirdparty')
        .test(/\.m?[jt]sx?$/)
        .use('babel-loader')
        .loader('babel-loader')
        .options({
          presets: [['@babel/preset-env', { targets: { chrome: 98 } }], 'babel-preset-typescript-vue3'],
          plugins: [["@vue/babel-plugin-jsx", { transformOn: true, mergeProps: true }], "@babel/plugin-transform-typescript"]
        })
        .end()
      config.module
        .rule('thirdparty').include.add(path.resolve(__dirname, '../../node_modules/@formily/vue'))

      // 不用esbuild打包
      config.module.rule('ts').uses.delete('esbuild-loader').end()
      return config
    },
  },
  themeConfig: {
    logo: '//img.alicdn.com/imgextra/i2/O1CN01Kq3OHU1fph6LGqjIz_!!6000000004056-55-tps-1141-150.svg',
    nav: [
      {
        text: '指南',
        link: '/guide/',
      },
      {
        text: '主站',
        link: 'https://v2.formilyjs.org',
      },
      {
        text: 'GITHUB',
        link: 'https://github.com/alibaba/formily',
      },
    ],
    sidebar: {
      '/guide/': ['', ...componentFiles],
    },
    lastUpdated: 'Last Updated',
    smoothScroll: true,
  },
  // plugins: [
  // 'vuepress-plugin-typescript',
  // [
  //   '@vuepress/register-components',
  //   {
  //     componentsDir: path.resolve(__dirname, './components'),
  //   },
  // ],
  // '@vuepress/back-to-top',
  // '@vuepress/last-updated',
  // [
  //   '@vuepress/medium-zoom',
  //   {
  //     selector: '.content__default :not(a) > img',
  //   },
  // ],
  // ],
}
