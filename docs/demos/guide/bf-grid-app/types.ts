import { RouteParams } from "vue-router"

export interface ILink {
    name: string
    slots: Array<'RecordForm.Toolbar' | 'a'>
    type: string
    label: string
    linkTo: string
    visible: (params: RouteParams, model: any) => boolean
    props: Record<any, any>
}

export interface IService {
    name: string
    type: string
    resolver: (params: RouteParams, model: any, action) => void
}

export type IAction = {
    name: string
    type: string
    service: string
    label: string
    visible: (params: RouteParams, model: any) => boolean
    disabled?: (params: RouteParams, model: any) => boolean
    slots: Array<'RecordForm.Toolbar' | 'a'>
    props?: Record<any, any>
} & Record<any, any>