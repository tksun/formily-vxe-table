import { Form } from '@formily/core'
import { ISchema } from '@formily/vue'
import { computed, defineComponent, inject, provide, ref } from 'vue'
import { EventEmitter } from 'events'

// 提供schema
export const BFGridSchemaProvider = defineComponent({
  name: 'BFGriddSchemaProvider',
  props: ['value'],
  setup(props, { slots }) {
    provide(
      Symbol.for('BFGridSchema'),
      computed(() => props.value)
    )
    return () => {
      return slots.default?.()
    }
  },
})
export function useBFGridSchema() {
  return inject(Symbol.for('BFGridSchema'), ref(null))
}
// 提供form实例
export const BFGridFormProvider = defineComponent({
  name: 'BFGridFormProvider',
  props: ['value'],
  setup(props, { slots }) {
    provide(
      Symbol.for('BFGridForm'),
      computed(() => props.value)
    )
    return () => {
      return slots.default?.()
    }
  },
})
export function useBFGridForm() {
  return inject(Symbol.for('BFGridForm'), ref(null))
}
// 提供领域模型
export const BFGridDomainProvider = defineComponent({
  name: 'BFGridDomainProvider',
  props: ['value'],
  setup(props, { slots }) {
    provide(
      Symbol.for('BFGridDomain'),
      computed(() => props.value)
    )
    return () => {
      return slots.default?.()
    }
  },
})
export function useBFGridDomain() {
  return inject(Symbol.for('BFGridDomain'), ref(null))
}
// 提供服务
export const BFGridServicesProvider = defineComponent({
  name: 'BFGridServicesProvider',
  props: ['value'],
  setup(props, { slots }) {
    provide(
      Symbol.for('BFGridServices'),
      computed(() => props.value)
    )
    return () => {
      return slots.default?.()
    }
  },
})
export function useBFGridServices() {
  return inject(Symbol.for('BFGridServices'), ref(null))
}

// 提供弹窗服务
export const BFGridDialogProvider = defineComponent({
  name: 'BFGridDialogProvider',
  props: ['value'],
  setup(props, { slots }) {
    provide(
      Symbol.for('BFGridDialog'),
      computed(() => props.value)
    )
    return () => {
      return slots.default?.()
    }
  },
})
export function useBFGridDialog() {
  return inject(Symbol.for('BFGridDialog'), ref(null))
}
