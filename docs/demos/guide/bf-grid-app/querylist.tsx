import Vue, {
  computed,
  defineComponent,
  getCurrentInstance,
  provide,
  ref,
  Ref,
  inject,
  watch,
} from 'vue'
import { createSchemaField, useField } from '@formily/vue'
import { observer } from '@formily/reactive-vue'
import {
  createForm,
  ObjectField,
  onFieldMount,
  onFieldReact,
  onFormInit,
} from '@formily/core'
import { RouteParams, useRoute, useRouter } from 'vue-router'
import {
  composeExport,
  resolveComponent,
} from '@formily/vxe-table/src/__builtins__'
import {
  useTableInstance,
  VXETablePropsSymbol,
} from '@formily/vxe-table/src/array-table'
import {
  Table,
  Column,
  Colgroup,
  Pager,
  Select,
  Option,
  Optgroup,
  Grid,
} from 'vxe-table'
import {
  BFGridServicesProvider,
  useBFGridDomain,
  BFGridDialogProvider,
} from './domain-base'
import { IService } from './types'
import { FormDialog } from '@formily/vxe-table/src'

// 这个是queryList组件
export default defineComponent({
  props: {
    routes: {}, // 路由触发事件
    views: { type: Array, default: () => [] }, // 所有对应的视图
    services: { type: Array, default: () => [] }, // 服务
  },
  setup(props, { slots }) {
    const prefixRef = inject('prefix', ref('bf'))

    function renderDialog(options) {
      const route = props.routes.find((route) => route.path == options.linkTo)
      const view = props.views.find((view) => view.name == route.view)
      console.log(view)
      FormDialog(
        {
          title: options.label || view.label,
          resize: true,
          showZoom: true,
          size: 'medium',
        },
        view.component
      ).open()
    }

    return () => {
      return (
        <BFGridServicesProvider value={props.services}>
          <BFGridDialogProvider value={renderDialog}>
            <div class={`${prefixRef.value}__grid`}>{slots.default?.()}</div>
          </BFGridDialogProvider>
        </BFGridServicesProvider>
      )
    }
  },
})
