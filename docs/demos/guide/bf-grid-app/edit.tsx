import { defineComponent, watch } from 'vue'
import { useBFGridDomain } from './domain-base'

export const EditComponent = defineComponent({
  setup() {
    const domainRef = useBFGridDomain()
    watch(
      domainRef,
      () => {
        console.log(domainRef.value)
      },
      { immediate: true }
    )
    return { domain: domainRef }
  },
  render() {
    return (
      <div id={this.domain} onClick={() => this.domain.refresh()}>
        1235
      </div>
    )
  },
})
