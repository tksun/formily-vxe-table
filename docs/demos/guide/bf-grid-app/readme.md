
# 动态列修改添加
- formEffect中添加响应的schema

# slots 通过properties实现
[x] empty
[x] form
[x] toolbar
[x] top
[x] bottom
[x] pager

# [x] props events透传
## 实现数据加载
[] proxy-config
- 需要同时同步 Formily form 实例的数据
[] pager-config

# [x]获取grid实例
```js
const instance = useTableInstance() // table的实例可以调用
onMounted(() => {
    console.log(instance.value.getTableData())
})
```
