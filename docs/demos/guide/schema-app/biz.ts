import { Form, ObjectField } from '@formily/core'
import { define, observable, action, autorun } from '@formily/reactive'
import { Ref, watch } from 'vue'
import { VxeTableInstance } from 'vxe-table'
import { EventEmitter } from 'events'

export default class DomainModel extends EventEmitter {
  form: Form = null
  formConfig = {
    data: {},
  }

  pagerConfig = {
    currentPage: 1,
    pageSize: 50,
    total: 0,
  }

  sortConfig = {}

  filterConfig = {}
  tableInstance: Ref<VxeTableInstance>

  constructor(tableInstance: Ref<VxeTableInstance>) {
    super()

    define(this, {
      formConfig: observable,
      pagerConfig: observable,
      proxyConfig: observable,
      sortConfig: observable,
      filterConfig: observable,
      additonalProps: observable.computed,
      handlePagerChange: action
    })

    this.tableInstance = tableInstance

    let dispose = watch(this.tableInstance, () => {
      this.emit('loaded')
      dispose()
    })
    this.on('loaded', async () => {
      const callback = await this.query({})
      this.form.setValues({ gridapp: callback.data })
      const pagerField = this.form.query('gridapp.pager').take() as ObjectField
      console.log(pagerField)
      pagerField.setValue({ currentPage: 1, pageSize: 10, total: callback.count })
      this.pagerConfig.total = callback.count
    })
  }

  get additionProps() {
    return {
      pagerConfig: this.pagerConfig,
      sortConfig: this.sortConfig,
      filterConfig: this.filterConfig,
    }
  }

  handlePagerChange({ currentPage, pageSize }) {
    this.pagerConfig.currentPage = currentPage
    this.pagerConfig.pageSize = pageSize
    console.log(currentPage, pageSize)
  }

  async query({ page, sort, sorts, filters, form }) {
    const data = [
      {
        id: 10001,
        name: 'Test1',
        nickname: 'T1',
        role: 'Develop',
        sex: 'Man',
        age: 28,
        address: 'Shenzhen',
      },
      {
        id: 10002,
        name: 'Test2',
        nickname: 'T2',
        role: 'Test',
        sex: 'Women',
        age: 22,
        address: 'Guangzhou',
      },
      {
        id: 10003,
        name: 'Test3',
        nickname: 'T3',
        role: 'PM',
        sex: 'Man',
        age: 32,
        address: 'Shanghai',
      },
      {
        id: 10004,
        name: 'Test4',
        nickname: 'T4',
        role: 'Designer',
        sex: 'Women',
        age: 23,
        address: 'Shenzhen',
      },
      {
        id: 10005,
        name: 'Test5',
        nickname: 'T5',
        role: 'Develop',
        sex: 'Women',
        age: 30,
        address: 'Shanghai',
      },
      {
        id: 10006,
        name: 'Test6',
        nickname: 'T6',
        role: 'Designer',
        sex: 'Women',
        age: 21,
        address: 'Shenzhen',
      },
      {
        id: 10007,
        name: 'Test7',
        nickname: 'T7',
        role: 'Test',
        sex: 'Man',
        age: 29,
        address: 'Shenzhen',
      },
      {
        id: 10008,
        name: 'Test8',
        nickname: 'T8',
        role: 'Develop',
        sex: 'Man',
        age: 35,
        address: 'Shenzhen',
      },
    ]
    return {
      data,
      count: 1000
    }
  }
  // 关联Formily的实例
  async attachForm(form: Form) {
    this.form = form
  }

  async queryAll({ page, sort, sorts, filters, form }) {
    return { data: [], count: 0 }
  }

  async delete({ body }) {
    return { status: 0 }
  }

  async save({
    body: { insertRecords, updateRecords, removeRecords, pendingRecords },
  }) {
    return { status: 0 }
  }

  handleExport() {
    console.log('handleExport', this.tableInstance.value)
    this.tableInstance.value.zoom()
  }

  handleImport() {
    console.log('handleImport')
  }
}
