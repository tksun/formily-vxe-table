# InputNumber

> 数字输入框

## JSON Schema 案例

<dumi-previewer demoPath="guide/input-number/json-schema" />

## API

参考 [https://element.eleme.io/#/zh-CN/component/input-number](https://element.eleme.io/#/zh-CN/component/input-number)
