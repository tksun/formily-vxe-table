# Input

> 文本输入框

## JSON Schema 案例

<dumi-previewer demoPath="guide/input/json-schema" />


## API

参考 [https://element.eleme.io/#/zh-CN/component/input](https://element.eleme.io/#/zh-CN/component/input)
