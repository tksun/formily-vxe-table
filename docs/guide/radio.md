# Radio

> 单选框

## JSON Schema 案例

<dumi-previewer demoPath="guide/radio/json-schema" />

## API

参考 [https://vxetable.cn/#/table/module/radio](https://vxetable.cn/#/table/module/radio)
API [https://vxetable.cn/#/radio-group/api](https://vxetable.cn/#/radio-group/api)
API [https://vxetable.cn/#/radio/api](https://vxetable.cn/#/radio/api)

### 扩展属性

| 属性名     | 类型                                                                              | 描述     | 默认值  |
| ---------- | --------------------------------------------------------------------------------- | -------- | ------- |
| options    | [RadioProps](https://vxetable.cn/#/radio-group/api)[] | 选项     | []      |
| optionType | default/button                                                                    | 样式类型 | default |
