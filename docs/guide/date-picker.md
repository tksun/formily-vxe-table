# DatePicker

> 日期选择器

## JSON Schema 案例

<dumi-previewer demoPath="guide/date-picker/json-schema" />


## API

参考 [https://element.eleme.io/#/zh-CN/component/date-picker](https://element.eleme.io/#/zh-CN/component/date-picker)
