# Switch

> 开关组件

## JSON Schema 案例

<dumi-previewer demoPath="guide/switch/json-schema" />

## API

参考 [https://element.eleme.io/#/zh-CN/component/switch](https://element.eleme.io/#/zh-CN/component/switch)
