# Password

> 密码输入框


## JSON Schema 案例

<dumi-previewer demoPath="guide/password/json-schema" />

## API

参考 [https://element.eleme.io/#/zh-CN/component/input](https://element.eleme.io/#/zh-CN/component/input)
