# Select

> 下拉框组件

## JSON Schema 同步数据源案例

<dumi-previewer demoPath="guide/select/json-schema-sync" />

## JSON Schema 异步联动数据源案例

<dumi-previewer demoPath="guide/select/json-schema-async" />

## API

参考 [https://vxetable.cn/#/table/module/select](https://vxetable.cn/#/table/module/select)

### 扩展属性

| 属性名  | 类型                                                                                       | 描述 | 默认值 |
| ------- | ------------------------------------------------------------------------------------------ | ---- | ------ |
| options | [SelectOptionProps](https://vxetable.cn/#/select/api)[] | 选项 | []     |
