# TimePicker

> 时间选择器

## JSON Schema 案例

<dumi-previewer demoPath="guide/time-picker/json-schema" />

## API

参考 [https://element.eleme.io/#/zh-CN/component/time-picker](https://element.eleme.io/#/zh-CN/component/time-picker)
