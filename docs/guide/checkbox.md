# Checkbox

> 复选框

## JSON Schema 案例

<dumi-previewer demoPath="guide/checkbox/json-schema" />


## API

参考 [https://element.eleme.io/#/zh-CN/component/checkbox](https://element.eleme.io/#/zh-CN/component/checkbox)

### 扩展属性

| 属性名     | 类型                                                                                       | 描述     | 默认值  |
| ---------- | ------------------------------------------------------------------------------------------ | -------- | ------- |
| options    | [CheckboxProps](https://element.eleme.io/#/zh-CN/component/checkbox#checkbox-attributes)[] | 选项     | []      |
| optionType | default/button                                                                             | 样式类型 | default |
